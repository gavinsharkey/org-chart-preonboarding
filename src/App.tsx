import { DemoWrapper } from "./demo_wrapper";

function App() {
  return (
    <div className="App">
      <DemoWrapper />
    </div>
  );
}

export default App;
