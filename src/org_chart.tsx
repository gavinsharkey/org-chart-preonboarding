import React, { useMemo, useState } from 'react'
import OrgChart from "@dabeng/react-orgchart";
import {SelectInput} from './components/SelectInput'

export interface IUser {
  id: string
  userName?: string
  orgId?: string
  selectedOrgId?: number
  followersCount?: number
  isUsingLdap?: boolean
  color?: string
  firstName: string
  lastName: string
  fullName?: string
  isGroupAdmin?: boolean
  avatar?: string
  picture?: string
  cover?: string
  email?: string
  phone?: string
  bio?: string
  title?: string
  addressSt1?: string
  addressSt2?: string
  addressCity?: string
  addressState?: string
  zipAddress?: string
  reportsToObj?: IUser
  organization?: string
  department?: string
  designation?: string
  addressZip?: string
  dueDate?: Date
  isExpert?: boolean
  categories?: string[]
  invitedOn?: string
  status?: string
  hideCancelIfAdmin?: boolean
  orgName?: string
  superAdmin?: boolean
  availability?: string
  online?: boolean
  groupStatus?: string
  followerCount?: number
  org?: any
  reactionsCount?: number
  postsCount?: number
  bookmarkId?: string
  teamMembers?: any
  coverMedia?: string
  pictureMedia?: string
  reportsTo?: string
  followedByMe?: boolean
}

const OrganizationEmployeeChart = (props: any) => {
  const { employees = [], hasSidebar = false } = props
  return (
    <div
      id='org-chart'
      className={`org-chart-wrapper pt-0 ${
        hasSidebar ? 'has-sidebar' : ''
      }`}
    >
      <Header orgName="JPAR - Dallas" />
      <OrgTree employees={employees} />
    </div>
  )
}

export default OrganizationEmployeeChart

const Header = ({ orgName }: {orgName: string}) => {
  return (
    <div className='org-chart-header'>
      <h3 id='org-chart-header'>{orgName}</h3>
      <div className='org-chart-options'>
        <div className='field is-sideways'>
          <label className='label'>Sub Org</label>

          <div className='control'>
            <div className='select'>
              <SelectInput
                name='orgsList'
                id='sub-org-select'
                options={[
                  {
                    label: orgName,
                    value: 'jp-dalas',
                    id: 'jp-dalas',
                  },
                ]}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

const OrgTree = ({ employees = [] }) => {
  const orgTree = useMemo(() => constructTree(employees), [employees])
  // @ts-ignore
  return (
    <div className='sub-org-carousel-wrapper'>
      <div className='carousel-slider' id='carousel-container'>
        <div className='sub-org-wrapper'>
          <div className='org-chart-body'>
            <div className='org-users'>
              <OrgChart
                datasource={orgTree}
                NodeTemplate={OrgUserBubble}
                pan
                zoom
                draggable
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
  // return
}

interface NodeData {
  nodeData: IUser & {
    children: NodeData[];
    isDisabled: boolean;
    isSelected?: boolean
  }
}

const OrgUserBubble = ({ nodeData }: NodeData) => {
  const [isToggleOpen, setIsToggleOpen] = useState(true)
  const {
    children = [],
    picture,
    department,
    designation = '',
    isDisabled = false,
    isSelected = false,
    id = ""
  } = nodeData || {}
  const showToggle = !!children.length
  const toggleCollapsedButton = () => setIsToggleOpen(prev => !prev)
  return (
    <div id={id} className={`org-user ${isDisabled ? 'disabled' : ''} ${isSelected ? "is-selected" : ""}`}>
      <div className='org-user-inner'>
        <div className='org-user-image'>
          <div className='org-user-image-inner'>
            <img
              src={picture}
              alt=''
              style={{height: "100%"}}
            />
          </div>
        </div>
        <div className='org-user-meta'>
          <p>{123}</p>
          <p>{department}</p>
          <small>
            <i>{designation}</i>
          </small>
        </div>
        {showToggle && (
          <div className='expand-org-button'>
            <button className='button is-small' onClick={toggleCollapsedButton}>
              <span className='icon'>
                <i
                  className={`fas ${
                    !isToggleOpen ? 'fa-chevron-down' : 'fa-chevron-up'
                  }`}
                ></i>
              </span>
            </button>
          </div>
        )}
      </div>
    </div>
  )
}

const ds = {
  id: 'n1',
  name: 'Lao Lao',
  title: 'general manager',
  children: [
    { id: 'n2', name: 'Bo Miao', title: 'department manager' },
    {
      id: 'n3',
      name: 'Su Miao',
      title: 'department manager',
      children: [
        { id: 'n4', name: 'Tie Hua', title: 'senior engineer' },
        {
          id: 'n5',
          name: 'Hei Hei',
          title: 'senior engineer',
          children: [
            { id: 'n6', name: 'Dan Dan', title: 'engineer' },
            { id: 'n7', name: 'Xiang Xiang', title: 'engineer' },
          ],
        },
        { id: 'n8', name: 'Pang Pang', title: 'senior engineer' },
      ],
    },
    { id: 'n9', name: 'Hong Miao', title: 'department manager' },
    {
      id: 'n10',
      name: 'Chun Miao',
      title: 'department manager',
      children: [{ id: 'n11', name: 'Yue Yue', title: 'senior engineer' }],
    },
  ],
}

const constructTree = (users) => {
  let root;
  users = users.map(user => ({...user, dbId: user.id, id: user.firstName + user.lastName, children: []}))

  for (let user of users ) {
    if (!user.reportsTo) {
      root = user;
      break
    }
  }

  const nextUsers = [root]

  while (nextUsers.length) {
    let currentUser = nextUsers.shift();
    users.forEach((user, i, users) => {
      if (user.reportsTo === currentUser.dbId) {
        currentUser.children.push(user);
        nextUsers.push(user)
        users = [...users.slice(0, i), ...users.slice(i+1)];
      }
    })
  }

  return root
}
