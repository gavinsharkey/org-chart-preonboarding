import React from 'react'

interface SelectInputProps extends React.ComponentPropsWithoutRef<"select"> {
  options: Array<{
    id?: string;
    value: string;
    label: string
  }>
}

export const SelectInput = React.forwardRef((props: SelectInputProps, ref: React.Ref<HTMLSelectElement>) => {
  const { options, ...rest } = props
  
  return <select ref={ref} {...rest}>
    {options.map(option => {
      return <option key={option.id} value={option.value}>
        {option.label}
      </option>
    })}
  </select>
})