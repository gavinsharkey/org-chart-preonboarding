const SVG_LOCAL = {
  excel: require('./xlsx.svg'),
  pdf: require('./pdf.svg'),
  ppt: require('./ppt.svg'),
  txt: require('./txt.svg'),
  word: require('./doc.svg'),
}

export default SVG_LOCAL
