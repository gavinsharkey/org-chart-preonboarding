import OrgChart from "./org_chart"

const EMPLOYEES = [
  {
    id: "b1c010b0-c434-11eb-b617-851854ddd100",
    createdAt: "2021-06-03T06:26:57.051Z",
    updatedAt: "2021-12-22T21:47:50.066Z",
    createdBy: "0a7a6b9f-e1ba-4b6b-be42-893dc066279c",
    updatedBy: null,
    orgId: "b1c0acf2-c434-11eb-b617-851854ddd100",
    parentOrgId: "b1c0acf2-c434-11eb-b617-851854ddd100",
    userName: "qa+cory@techrivers.com",
    email: "qa+cory@techrivers.com",
    bookmarkId: null,
    status: "ACTIVE",
    firstName: "Cory",
    lastName: "Zimmerman",
    age: null,
    picture:
      "https://images.big.fish/eyJidWNrZXQiOiJib3Nzcy1pbWFnZSIsImtleSI6ImRldmVsb3BtZW50L28tYjFjMGFjZjItYzQzNC0xMWViLWI2MTctODUxODU0ZGRkMTAwL20tMTYvdS1iMWMwMTBiMC1jNDM0LTExZWItYjYxNy04NTE4NTRkZGQxMDAvMDM2NTM4ZjE2ZjliNmZmZjVkMWRjZjcxZDgzMmQ3ODAzZWM0MjAyNy5qcGVnIn0=",
    cover: null,
    bio: null,
    phone: null,
    addressSt1: null,
    addressSt2: null,
    addressCity: null,
    addressState: null,
    addressZip: null,
    reportsTo: null,
    designation: "Radio Announcer",
    department: null,
    color: "#DC814E",
    isExpert: false,
    categories: [],
    invitedOn: "2021-06-03T06:26:57.757Z",
    lastLogin: "2021-12-22T21:47:50.014Z",
    online: false,
    followersCount: 0,
    availability: "ACTIVE",
    orgName: "Apple Inc.",
    org: {
      id: "b1c0acf2-c434-11eb-b617-851854ddd100",
      name: "Apple Inc.",
    },
    availabilityStatusText: {
      key: "availabilityStatusText",
      obj: "{addressCity: null, addressSt1: null, addressSt2: n…}",
      type: 0,
      options: "{enableCircularCheck: false, enableImplicitConversi…}",
    },
    locationId: null,
    reactionsCount: 1,
    postsCount: 17,
    meta: {
      invitedOn: "2021-06-03T06:26:57.757Z",
      lastLogin: "2021-12-22T21:47:50.014Z",
      invitationAccepted: "2021-06-03T06:28:14.565Z",
    },
    isDisabled: false,
  },
  {
    id: "1298d270-c438-11eb-b617-851854ddd100",
    createdAt: "2021-06-03T06:51:07.485Z",
    updatedAt: "2021-12-22T21:47:19.756Z",
    createdBy: "b1c010b0-c434-11eb-b617-851854ddd100",
    updatedBy: null,
    orgId: "b1c0acf2-c434-11eb-b617-851854ddd100",
    parentOrgId: "b1c0acf2-c434-11eb-b617-851854ddd100",
    userName: "qa+crockett@techrivers.com",
    email: "qa+crockett@techrivers.com",
    bookmarkId: null,
    status: "ACTIVE",
    firstName: "Crystal",
    lastName: "Crockett",
    age: null,
    picture:
      "https://images.big.fish/eyJidWNrZXQiOiJib3Nzcy1pbWFnZSIsImtleSI6ImRldmVsb3BtZW50L28tYjFjMGFjZjItYzQzNC0xMWViLWI2MTctODUxODU0ZGRkMTAwL20tMTYvdS0xMjk4ZDI3MC1jNDM4LTExZWItYjYxNy04NTE4NTRkZGQxMDAvMmU4NjMzYTUzMDVlZWMyYjdhM2Q5ODI3YTMwOWI5ZDBkNzA5MzYxMi5qcGcifQ==",
    cover: null,
    bio: null,
    phone: null,
    addressSt1: "3195",
    addressSt2: "Harron Drive",
    addressCity: "Pikesville",
    addressState: "MD",
    addressZip: "21208",
    reportsTo: "b1c010b0-c434-11eb-b617-851854ddd100",
    designation: "Postsecondary",
    department: null,
    color: "#EFB422",
    isExpert: false,
    categories: [],
    invitedOn: "2021-06-03T06:51:07.617Z",
    lastLogin: "2021-12-22T21:47:19.717Z",
    online: false,
    followersCount: 0,
    availability: "ACTIVE",
    orgName: "Apple Inc.",
    org: {
      id: "b1c0acf2-c434-11eb-b617-851854ddd100",
      name: "Apple Inc.",
    },
    availabilityStatusText: {
      key: "availabilityStatusText",
      obj: '{addressCity: "Pikesville", addressSt1: "3195", add…}',
      type: 0,
      options: "{enableCircularCheck: false, enableImplicitConversi…}",
    },
    locationId: null,
    reactionsCount: 0,
    postsCount: 8,
    meta: {
      invitedOn: "2021-06-03T06:51:07.617Z",
      lastLogin: "2021-12-22T21:47:19.717Z",
      invitationAccepted: "2021-06-03T07:00:18.441Z",
    },
    isDisabled: false,
  },
  {
    id: "dc558690-c437-11eb-b617-851854ddd100",
    createdAt: "2021-06-03T06:49:36.447Z",
    updatedAt: "2021-07-09T05:51:06.539Z",
    createdBy: "b1c010b0-c434-11eb-b617-851854ddd100",
    updatedBy: null,
    orgId: "b1c0acf2-c434-11eb-b617-851854ddd100",
    parentOrgId: "b1c0acf2-c434-11eb-b617-851854ddd100",
    userName: "qa+dennis@techrivers.com",
    email: "qa+dennis@techrivers.com",
    bookmarkId: null,
    status: "ACTIVE",
    firstName: "Dennis",
    lastName: "Kelly",
    age: null,
    picture:
      "https://images.big.fish/eyJidWNrZXQiOiJib3Nzcy1pbWFnZSIsImtleSI6ImRldmVsb3BtZW50L28tYjFjMGFjZjItYzQzNC0xMWViLWI2MTctODUxODU0ZGRkMTAwL20tMTYvdS1kYzU1ODY5MC1jNDM3LTExZWItYjYxNy04NTE4NTRkZGQxMDAvYzJhZjAyNWVkNDYxZGNlM2RhYmRlMjhiODgzNmM0OGNhYjVjOTMwOS5qcGVnIn0=",
    cover: null,
    bio: null,
    phone: null,
    addressSt1: "1213",
    addressSt2: "Berry Street",
    addressCity: "Sheridan Lake",
    addressState: "CO",
    addressZip: "81071",
    reportsTo: "b1c010b0-c434-11eb-b617-851854ddd100",
    designation: "Operator",
    department: null,
    color: "#0A208E",
    isExpert: false,
    categories: [],
    invitedOn: "2021-06-03T06:49:36.574Z",
    lastLogin: "2021-06-29T12:40:25.353Z",
    online: false,
    followersCount: 0,
    availability: "ACTIVE",
    orgName: "Apple Inc.",
    org: {
      id: "b1c0acf2-c434-11eb-b617-851854ddd100",
      name: "Apple Inc.",
    },
    availabilityStatusText: {
      key: "availabilityStatusText",
      obj: '{addressCity: "Sheridan Lake", addressSt1: "1213", …}',
      type: 0,
      options: "{enableCircularCheck: false, enableImplicitConversi…}",
    },
    locationId: null,
    reactionsCount: 0,
    postsCount: 0,
    meta: {
      invitedOn: "2021-06-03T06:49:36.574Z",
      lastLogin: "2021-06-29T12:40:25.353Z",
      invitationAccepted: "2021-06-03T06:57:09.564Z",
    },
    isDisabled: false,
  },
  {
    id: "87bbcf90-c437-11eb-b617-851854ddd100",
    createdAt: "2021-06-03T06:47:14.586Z",
    updatedAt: "2021-07-06T06:46:09.801Z",
    createdBy: "b1c010b0-c434-11eb-b617-851854ddd100",
    updatedBy: null,
    orgId: "b1c0acf2-c434-11eb-b617-851854ddd100",
    parentOrgId: "b1c0acf2-c434-11eb-b617-851854ddd100",
    userName: "qa+rayner@techrivers.com",
    email: "qa+rayner@techrivers.com",
    bookmarkId: null,
    status: "ACTIVE",
    firstName: "Richard",
    lastName: "Rayner",
    age: null,
    picture:
      "https://images.big.fish/eyJidWNrZXQiOiJib3Nzcy1pbWFnZSIsImtleSI6ImRldmVsb3BtZW50L28tYjFjMGFjZjItYzQzNC0xMWViLWI2MTctODUxODU0ZGRkMTAwL20tMTYvdS04N2JiY2Y5MC1jNDM3LTExZWItYjYxNy04NTE4NTRkZGQxMDAvM2Y3M2QyNjc4NTJmMWNjMDVlMWVmNWE1MGVjNjJhMjJjMWQxNTg1NC5qcGVnIn0=",
    cover: null,
    bio: null,
    phone: null,
    addressSt1: "3661",
    addressSt2: "Old Dear Lane",
    addressCity: "KINGSPORT",
    addressState: "TN",
    addressZip: "37665",
    reportsTo: "1298d270-c438-11eb-b617-851854ddd100",
    designation: "Maintenance Worker",
    department: null,
    color: "#808000",
    isExpert: false,
    categories: [],
    invitedOn: "2021-06-03T06:47:14.727Z",
    lastLogin: "2021-06-29T12:40:01.513Z",
    online: false,
    followersCount: 0,
    availability: "ACTIVE",
    orgName: "Apple Inc.",
    org: {
      id: "b1c0acf2-c434-11eb-b617-851854ddd100",
      name: "Apple Inc.",
    },
    availabilityStatusText: {
      key: "availabilityStatusText",
      obj: '{addressCity: "KINGSPORT", addressSt1: "3661", addr…}',
      type: 0,
      options: "{enableCircularCheck: false, enableImplicitConversi…}",
    },
    locationId: null,
    reactionsCount: 2,
    postsCount: 4,
    meta: {
      invitedOn: "2021-06-03T06:47:14.727Z",
      lastLogin: "2021-06-29T12:40:01.513Z",
      invitationAccepted: "2021-06-03T06:52:35.256Z",
    },
    isDisabled: false,
    isSelected: true
  },
  {
    id: "ad110030-c437-11eb-b617-851854ddd100",
    createdAt: "2021-06-03T06:48:17.219Z",
    updatedAt: "2021-06-29T12:40:36.406Z",
    createdBy: "b1c010b0-c434-11eb-b617-851854ddd100",
    updatedBy: null,
    orgId: "b1c0acf2-c434-11eb-b617-851854ddd100",
    parentOrgId: "b1c0acf2-c434-11eb-b617-851854ddd100",
    userName: "qa+robert@techrivers.com",
    email: "qa+robert@techrivers.com",
    bookmarkId: null,
    status: "ACTIVE",
    firstName: "Robert",
    lastName: "McDuffie",
    age: null,
    picture:
      "https://images.big.fish/eyJidWNrZXQiOiJib3Nzcy1pbWFnZSIsImtleSI6ImRldmVsb3BtZW50L28tYjFjMGFjZjItYzQzNC0xMWViLWI2MTctODUxODU0ZGRkMTAwL20tMTYvdS1hZDExMDAzMC1jNDM3LTExZWItYjYxNy04NTE4NTRkZGQxMDAvYmU3Y2ExMDUxMzhlZmI5YTQ5YjRmZjQwNjExZmYyOGRmMzBjN2MyNS5qcGVnIn0=",
    cover: null,
    bio: null,
    phone: "",
    addressSt1: "2501",
    addressSt2: "Chatham Way",
    addressCity: "Reston",
    addressState: "MD",
    addressZip: "20191",
    reportsTo: "dc558690-c437-11eb-b617-851854ddd100",
    designation: "Orthotist and Prosthetist",
    department: null,
    color: "#808000",
    isExpert: false,
    categories: [],
    invitedOn: "2021-06-03T06:48:17.347Z",
    lastLogin: "2021-06-29T12:39:37.092Z",
    online: false,
    followersCount: 0,
    availability: "ACTIVE",
    orgName: "Apple Inc.",
    org: {
      id: "b1c0acf2-c434-11eb-b617-851854ddd100",
      name: "Apple Inc.",
    },
    availabilityStatusText: {
      key: "availabilityStatusText",
      obj: '{addressCity: "Reston", addressSt1: "2501", address…}',
      type: 0,
      options: "{enableCircularCheck: false, enableImplicitConversi…}",
    },
    locationId: null,
    reactionsCount: 0,
    postsCount: 0,
    meta: {
      invitedOn: "2021-06-03T06:48:17.347Z",
      lastLogin: "2021-06-29T12:39:37.092Z",
      invitationAccepted: "2021-06-03T06:55:02.994Z",
    },
    isDisabled: false,
    relationship: "000",
  },
];

export function DemoWrapper() {
  return (
    <div className="signup-wrapper">
      <div className="fake-nav">
        <a href="/" className="logo">
          <img
            src="assets/img/logo/friendkit-bold.svg"
            width="112"
            height="28"
            alt=""
          />
        </a>
      </div>
      <div className="process-bar-wrap">
        <div className="process-bar">
          <div className="progress-wrap">
            <div className="track"></div>
            <div className="bar" style={{width: "50%"}}></div>
            <div
              id="step-dot-1"
              className="dot is-first is-active is-current"
              data-step="0"
            >
              <i data-feather="user"></i>
            </div>
            <div id="step-dot-2" className="dot is-active is-second" data-step="25">
              <i data-feather="image"></i>
            </div>
            <div id="step-dot-3" className="dot is-active is-third" data-step="50">
              <i data-feather="lock"></i>
            </div>
            <div id="step-dot-4" className="dot is-fourth" data-step="75">
              <i data-feather="link"></i>
            </div>
            <div id="step-dot-5" className="dot is-fifth" data-step="100">
              <i data-feather="flag"></i>
            </div>
          </div>
        </div>
      </div>
      <div className="outer-panel">
        <div className="outer-panel-inner">
          <div className="process-title">
            <div id="step-title-3" className="step-title is-active">
              <h2>Organization Location</h2>
              <p>
                Please select where this user is location in the organization.
                By default, they will be placed beneath you.
              </p>
            </div>
          </div>
          <div id="signup-panel-3" className="process-panel-wrap is-wide is-active">
            <OrgChart employees={EMPLOYEES} />

            <div className="buttons">
              <a
                className="button is-rounded process-button"
                data-step="step-dot-2"
              >
                Back
              </a>
              <a
                className="button is-rounded process-button is-next"
                data-step="step-dot-4"
              >
                Next
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
