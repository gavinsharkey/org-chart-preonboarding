import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import "./assets/css/bulma.css"
import "./assets/css/core.css"
import "./assets/css/fontawesome/all.css"
import './index.css'

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
